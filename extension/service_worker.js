const scripts = ["js/blacklist2.js", "js/lib/chrono.min.js", "js/textprocessing.js", "js/queryparser.js", "js/background.js"];

scripts.map(script => {
    importScripts(script);
});