const MILLIS_BEFORE_CLEAR = 1000 * 60; // 60 seconds
const CLEAR_DELAY = 20000;
const MAX_URL_LEN_SHOWN = 50;
const LT = (a,b) => {return a < b};
const GT = (a,b) => {return a > b};

const LT_OBJ = (a,b) => {
    return a.time < b.time;
}

const GT_OBJ = (a,b) => {
    return a.time > b.time;
}

Array.max = ( array ) => {
    return Math.max.apply(Math,array);
};

const ValidURL = (text) => {
    const valid = /((https?):\/\/)?(([w|W]{3}\.)+)?[a-zA-Z0-9\-\.]{3,}\.[a-zA-Z]{2,}(\.[a-zA-Z]{2,})?/
    return valid.test(text);
}

chrome.omnibox.onInputChanged.addListener(omnibarHandler);
chrome.omnibox.onInputEntered.addListener(acceptInput);
chrome.runtime.onMessage.addListener(handleMessage);

const injectScript = (tabId) => {
    chrome.scripting.executeScript(
        {
            target: {tabId: tabId},
            files: ['./js/inject.js']
        }
    )
}

chrome.tabs.onUpdated.addListener((tabId, changeInfo, tab) => {
    if(tab.url && changeInfo.status === "complete"){
        if(tab.url.includes('chrome-extension://') || tab.url.includes('chrome://')){
            return;
        }
        else {
            injectScript(tabId);
        }
    }
    
})

chrome.runtime.onInstalled.addListener( (object) => {
    chrome.storage.local.get("shouldOpenTab", (item) => {
        if (Object.keys(item).length == 0) {
            // This used to show irelevant code repo annoyingly( not well thought)
            // chrome.tabs.create({url: "https://gitlab.com/folovco/index-and-search-pages"}, function (tab) {});
            chrome.storage.local.set({"shouldOpenTab": {"dontShow": true}})
            // Log does not show there but let's keep it..:
            console.log("I do consider onboarding run/version of preferences, explaing the options a bit, but also allowing experienced users to skip them quickly.")
        }
    })
});

function acceptInput(text, disposition) {
    // disposition: "currentTab", "newForegroundTab", or "newBackgroundTab"
    if (!ValidURL(text)) {
        return;
    }
    switch (disposition) {
    case "currentTab":
        chrome.tabs.update({url: text});
        break;
    case "newForegroundTab":
        chrome.tabs.create({url: text});
        break;
    case "newBackgroundTab":
        chrome.tabs.create({url: text, active: false});
        break;
    }
}

function init() {
    const window = self;
    window.preloaded = [];
    window.cache = {};
    chrome.storage.local.get(['blacklist', 'preferences'], function(items) {
        let obj = items['blacklist'];
        if (obj === undefined || !('PAGE' in obj && 'SITE' in obj && 'REGEX' in obj)) {
            window.blacklist = {'PAGE':[], 'REGEX':[], 'SITE':[]}; // show example in page
            chrome.storage.local.set({'blacklist':blacklist});
        } else {
            window.blacklist = obj;
        }

        obj = items['preferences'];
        if (obj === undefined) {
            window.preferences = {};
            chrome.storage.local.set({'preferences':preferences});
        } else {
            window.preferences = obj;
        }
    });

    chrome.storage.local.get('index', function(items) {
        const obj = items['index'];
        if (obj === undefined) {
            window.timeIndex = [];
            chrome.storage.local.get(null, function(items) {
                for (var key in items) {
                    if (key != 'index') {
                        if(typeof items[key].time === "string" && items[key].time !== "")
                            timeIndex.push(items[key].time.toString());
                    }
                }

                timeIndex.sort(function(a,b) {return parseInt(a) - parseInt(b)}); // soonest last
                makePreloaded(timeIndex);
                chrome.storage.local.set({'index':{'index':timeIndex}});
            });

        } else {
            window.timeIndex = obj.index;
            if(typeof window.timeIndex === "undefined")
                window.timeIndex = [];
            makePreloaded(timeIndex);
        }
    });
}

function makePreloaded(index) {
    const window = self;
    const preloaded_index = [];
    const millis = +CUTOFF_DATE;
    const i = Math.floor(binarySearch(index, millis, LT, GT, 0, index.length));
    for (let j = i; j < index.length; j++) {
        preloaded_index.push(index[j]);
    }

    chrome.storage.local.get(preloaded_index, function(items) {
        window.preloaded = [];
        for (let key in items) {
            preloaded.push(items[key]);
        }
        preloaded.sort(function(a,b){return a.time-b.time});
    });

    chrome.storage.local.set({'preloaded': preloaded});
}

function assert(condition, message) {
    if (!condition) {
        throw message || "Assertion failed";
    }
}

function handleMessage(data, sender, sendResponse) {
      // data is from message
    if (data.msg === 'pageContent' && shouldArchive(data)) {
        delete data.msg;
        data.text = processPageText(data.text);
        const time = data.time;
        const keyValue = {};
        keyValue[time] = data;

        let promise = new Promise((resolve, reject) => {
            if(Array.isArray(self.preloaded)) {
                for(let i = 0; i < preloaded.length; i++) {
                    if(data.url === preloaded[i]["url"]) {
                        resolve(true);
                        break;
                    }
                } 
                resolve(false);
            }
        })
        promise.then((resultValue) => {
            if(!resultValue) {
                if(Array.isArray(self.timeIndex))
                    timeIndex.push(time.toString());
                if(Array.isArray(self.preloaded))
                    preloaded.push(data);

                chrome.storage.local.set(keyValue, function() {
                    console.log("Stored: " + data.title);
                });
                chrome.storage.local.set({'index':{'index':self.timeIndex}});
            }
        }).catch(error => {
           
        })

        
        
    } else if (data.msg === 'setPreferences') {
        self.preferences = data.preferences;
        chrome.storage.local.set({'preferences':self.preferences});
    } else if (data.msg === 'setBlacklist') {
        self.blacklist = data.blacklist;
        chrome.storage.local.set({'blacklist':self.blacklist});
    }
}

function omnibarHandler(text, suggest) {
    dispatchSuggestions(text, suggestionsComplete, suggest);
}

function suggestionsComplete(suggestions, shouldDate, suggestCb) {
    const window = self;
    const res = [];
    for (let i = 0; i < suggestions.length; i++) {
        const elem = suggestions[i];
        let urlToShow = elem.url;
        if (urlToShow.length >= MAX_URL_LEN_SHOWN) {
            urlToShow = urlToShow.substring(0,47) + '...';
        }
        let description = "<url>" + escape(urlToShow) + "</url> "
        const date = new Date(elem.time);
        let hour = date.getHours();
        if (hour > 12) {
            hour -= 12;
            if (hour === 12) {
                hour = hour.toString + 'am';
            } else {
                hour = hour.toString() + "pm";
            }
        } else {
            if (hour === 12) {
                hour = hour.toString() + "pm";
            } else {
                hour = hour.toString() + "am";
            }
        }

        const fmt =  (date.getMonth() + 1) + '/' + date.getDate() + '/' + date.getUTCFullYear().toString().substring(2,4);
        if (shouldDate) {
            description += ':: <match>' + escape(fmt + " " + hour) + '</match> ';
        } else {
            description += ':: ' + escape(fmt) + ' ';
        }

        description += '- ' + escape(elem.title);
        res.push({content:elem.url, description:description});
    }
    if (res.length > 0) {
        chrome.omnibox.setDefaultSuggestion({description: "Select an option below"});
    } else {
        chrome.omnibox.setDefaultSuggestion({description: "No results found"})
    }
    suggestCb(res);
    window.setTimeout(clearCache, CLEAR_DELAY);
}

function clearCache() {
    return;
    var now = +(new Date());

    for (var time in cache) {
        if (now - parseInt(time) > MILLIS_BEFORE_CLEAR) {
            delete cache[time];
        }
    }
}

function escapeRegExp(str) {
    return str.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&");
}

function shouldArchive(data) {
    // const blacklist = chrome.storage.local.get(['blacklist']);
    // console.log(blacklist);
    // blacklist =  {"REGEX", "PAGE", "SITE"}
    // custom / regex, DEFAULT_BLACKLIST
    
    let blacklist = self.blacklist;
    if(!blacklist || typeof blacklist["SITE"] === "undefined")
        blacklist = {'PAGE':[], 'REGEX':[], 'SITE':[]};

    
    const site = blacklist["SITE"];
    const page = blacklist["PAGE"];
    const regex = blacklist["REGEX"];
    const url = data.url;

    for (let i = 0; i < site.length; i++) {
        // var reg = new RegExp(escapeRegExp(page[i]) + ".*");
        if (url.indexOf(site[i].replace("http://",  "").replace("https://", "")) != -1) {
            return false;
        }
    }

    for (let i = 0; i < page.length; i++) {
        if (cleanURL(data.url).indexOf(page[i].replace("http://",  "").replace("https://", "")) != -1) {
            return false;
        }
    }

    for (let i = 0; i < regex.length; i++) {
        if (url.match(regex[i]) != null) {
            return false;
        }
    }

    return true;
}

function makeSuggestions(query, candidates, cb, suggestCb) {
    const res = [];
    const urls = {};
    const keywords = query.keywords;
    const keywordsLen = keywords.length;
    const negative = query.negative;
    const negativeLen = negative.length;
    let j = 0;
    for (let i = candidates.length - 1; i > -1; i--) {
        let text = candidates[i].text;
        let isMatching = true;
        for (let k = 0; k < negativeLen; k++) {
            if (text.indexOf(negative[k]) > -1) {
                isMatching = false;
            }
        }

        if (isMatching) {
            for (let k = 0; k < keywordsLen; k++) {
                if (text.indexOf(keywords[k]) === -1) {
                    isMatching = false;
                    break;
                }
            }

            if (isMatching) {
                let cleanedURL = cleanURL(candidates[i].url);
                if (!(cleanedURL in urls)) {
                    res.push(candidates[i]);
                    urls[cleanedURL] = true;
                    j += 1;
                    if (j === 6) {
                        break;
                    }
                }
            }
        }
    }

    cb(res,query.shouldDate,suggestCb);
}

function cleanURL(url) {
    return url.trim().replace(/(#.+?)$/, '');
}

function dispatchSuggestions(text, cb, suggestCb) {
    const query = makeQueryFromText(text);
    const window = self;
    query.text = text;
    if (query.before !== false && query.after !== false && query.after >= query.before) return;

    query.keywords.sort(function(a,b){return b.length-a.length});

    if (query.after >= CUTOFF_DATE) {
        
        let start = Math.floor(binarySearch(preloaded, {'time':+query.after}, LT_OBJ,
                                            GT_OBJ, 0, preloaded.length));

        let end;
        if (query.before) {
            end = Math.ceil(binarySearch(preloaded, {'time':+query.before}, LT_OBJ,
                                         GT_OBJ, 0, preloaded.length));
        } else {
            end = preloaded.length;
        }

        makeSuggestions(query, preloaded.slice(start, end), cb, suggestCb)
    } else {
        let timeIndex = self.timeIndex;
        let start = Math.floor(binarySearch(timeIndex, +query.after, LT,
                                            GT, 0, timeIndex.length));
        let end;
        if (query.before) {
            end = Math.ceil(binarySearch(timeIndex, +query.before, LT,
                                         GT, 0, timeIndex.length));
        } else {
            end = timeIndex.length;
        }

        window.sorted = [];
        let get = timeIndex.slice(start, end);
        let index = Math.ceil(binarySearch(get, +CUTOFF_DATE, LT, GT, 0, get.length));
        if (index < get.length) {
            sorted = preloaded.slice(0, get.length - index + 1);
        }
        get = get.slice(0,index);

        chrome.storage.local.get(get, function(items) {
            for (var key in items) {
                sorted.push(items[key]);
            }
            sorted.sort(function(a,b) {return a.time - b.time});
            makeSuggestions(query, sorted, cb, suggestCb);
        });
    }
}

function binarySearch(arr, value, lt, gt, i, j) {
    if (Math.abs(j - i) <= 1) {
        return (i + j)/2;
    }

    const m = Math.floor((i + j)/2)
    const cmpVal = arr[m];
    if (gt(cmpVal, value)) {
        j = m;
    } else if (lt(cmpVal, value)){
        i = m;
    } else {
        return m;
    }
    return binarySearch(arr, value, lt, gt, i, j);
}

init();
