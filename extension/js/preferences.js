(function() {
    var allPageDisplay = null;

    var add = function(type="SITE", content) {
        if(content === "chrome-ui://newtab")
            return;
        var tab = document.getElementById("blacklist_tbl")
        var row = tab.insertRow()
        var stringCell = row.insertCell()
        stringCell.innerHTML = content ? content : ""
        stringCell.contentEditable = true
        stringCell.setAttribute("placeholder", "Add a site...");

        var typeCell = row.insertCell()
        var selectCell = document.createElement('select');
        selectCell.innerHTML = '<option value="PAGE">Specific Page</option> \
                        <option value="SITE" selected="true">Entire Website</option> \
                        <option value="REGEX">Regex</option>';
        selectCell.value = type

        typeCell.appendChild(selectCell);

        var enabledCell = row.insertCell()
        enabledCell.innerHTML = "<input type='checkbox' checked></input>"
        var deleteThisCell = document.createElement("a");
        deleteThisCell.classList = ["delete"];
        deleteThisCell.innerHTML = `<span style="font-size: 14px">Delete</span>`
        deleteThisCell.onclick = function(e) {
            var r = e.target.parentElement.parentElement.parentElement
            r.parentNode.removeChild(r);
        }
        enabledCell.appendChild(deleteThisCell);
    }

    function cutString(stringToCut) {
        if (stringToCut.length == 0)
            return "<em>No title</em>"
        if (stringToCut.length <= 50)
            return stringToCut
        return stringToCut.slice(0, 50) + "..."
    }

    function addHistoricPages(pages) {
        console.log(pages);
        var history_table = document.getElementById("history_tbl")
        for(i in pages) {
            var thisRow = document.createElement("tr")
            var colOne = document.createElement("td")
            colOne.innerText =  cutString(pages[i].title) 
            var colTwo = document.createElement("td")
            let colTwoLink = document.createElement("a")
            colTwoLink.href = pages[i].url
            colTwoLink.innerText = cutString(pages[i].url)
            colTwoLink.target = "_blank"
            colTwo.appendChild(colTwoLink)
            thisRow.appendChild(colOne)
            thisRow.appendChild(colTwo)
            var deletePage = document.createElement("td")
            var deleteButton = document.createElement("a")
            deleteButton.classList = ["delete"];
            deleteButton.innerHTML = `<span style="font-size: 14px">Delete</span>`
            deleteButton.onclick = function(e) {
                var r = e.target.parentElement.parentElement.parentElement
                chrome.storage.local.remove(r.id)
                notie.alert(4, "Page deleted.", 2)
                r.parentNode.removeChild(r)
            }
            deletePage.appendChild(deleteButton)
            thisRow.appendChild(deletePage)
            thisRow.id = pages[i].time;
            history_table.appendChild(thisRow)
        }
    }

    function getHistory(query="") {
        var history_table = document.getElementById("history_tbl")
        history_table.innerHTML = "<table class='ui table' id='history_tbl'></table>"
        chrome.storage.local.get(function(results) {
            var allPages = []
            
            for (key in results) {
                if (!isNaN(key) && (results[key].url + "/" + results[key].title).indexOf(query) > -1) {
                    allPages.push(results[key])
                }
            }
            allPages.reverse()
            allPageDisplay = nextPages(allPages)
            addHistoricPages(allPageDisplay.next().value)
        })
    }

    function* nextPages(allPages){
        while(true)
            yield allPages.splice(0, 20)
    }

    chrome.storage.local.get('blacklist', function(result) {
        var bl = result.blacklist
        if (Object.keys(bl).length > 0 && (bl['SITE'].length + bl['PAGE'].length + bl['REGEX'].length > 0)) {
            var tab = document.getElementById("blacklist_tbl")
            var fields = ["SITE", "PAGE", "REGEX"]
            for (var j = 0; j < fields.length; j++) {
                for (var i = 0; i < bl[fields[j]].length; i++) {
                    add(fields[j], bl[fields[j]][i])
                }
            }
        } else {
            add("SITE", "chrome-ui://newtab");
            save(false);
        }
    });

    function save(showAlert) {
        var showAlert = (typeof showAlert !== 'undefined') ?  showAlert : true;
        if (showAlert) { notie.alert(4, "Saved Preferences.", 2); }
        var tab = document.getElementById("blacklist_tbl");
        var indices = [];
        for (var i = 1; i < tab.rows.length; i++) {
            var row = tab.rows[i]
            if (row.cells[0].innerText === "") {
                indices.push(i)
            }
        }

        for (var j = indices.length-1; j > -1; j--) {
            tab.deleteRow(indices[j]);
        }



        if (tab.rows.length == 1) {
            chrome.runtime.sendMessage({
                "msg": 'setBlacklist',
                "blacklist": []
            });
            add("SITE", "");
        } else {
            var b = {
                'SITE': [],
                'PAGE': [],
                'REGEX': []
            }
            for(var i = 1; i < tab.rows.length; i++) {
                b[tab.rows[i].cells[1].childNodes[0].value].push(tab.rows[i].cells[0].innerText)
            }

            chrome.runtime.sendMessage({
                "msg": 'setBlacklist',
                "blacklist": b
            })
        }
    }

    function loadMore() {
        addHistoricPages(allPageDisplay.next().value)
    }

    function clearAllData() {
        chrome.storage.local.clear();
        notie.alert(1, 'Deleted All Data. Restarting the extension...', 2)
        setTimeout(function() {
            chrome.runtime.reload()
        }, 2000);
    }

    function clearRules() {
        chrome.storage.local.get(['blacklist'], function(items) {
            var blacklist = items['blacklist'];
            blacklist['SITE'] = ['chrome-ui://newtab']
            chrome.storage.local.set({'blacklist':blacklist});
        });
        notie.alert(1, 'Deleted Rules. Restarting the extension...', 2)
        setTimeout(function() {
            chrome.runtime.reload()
        }, 2000);
    }

    function clearHistory() {
        chrome.storage.local.get(function(results) {
            // var timestaps = results['index']['index'];
            for(key in results){
                if(!isNaN(key))
                    chrome.storage.local.remove(key);
            }
            chrome.storage.local.set({'index':{'index':[]}});
        });
        notie.alert(1, 'Deleted History. Restarting the extension...', 2)
        setTimeout(function() {
            chrome.runtime.reload()
        }, 2000);
    }

    getHistory()

    document.getElementById("save").onclick = save;
    document.getElementById("add").onclick = function() {
        add("SITE", "");
    };
    document.getElementById("loadmore").onclick = loadMore;

    document.getElementById("clear").onclick = function () {
        notie.confirm('Are you sure you want to do that?', 'Yes', 'Cancel', function() {
            clearAllData();
        });
    }

    document.getElementById("clear-rules").onclick = function () {
        notie.confirm('Are you sure you want to do that?', 'Yes', 'Cancel', function() {
            clearRules();
        });
    }

    document.getElementById("clear-history").onclick = function () {
        notie.confirm('Are you sure you want to do that?', 'Yes', 'Cancel', function() {
            clearHistory();
        });
    }

    document.getElementById("export-history").onclick = function () {
        chrome.storage.local.get(function(results) {
            console.log(results);
            notie.alert(4, "Exported User Histories.", 2);
            const dataStr = "data:text/json;charset=utf-8," + encodeURIComponent(JSON.stringify(results));
            const downloadAnchorNode = document.createElement('a');
            downloadAnchorNode.setAttribute("href",  dataStr);
            downloadAnchorNode.setAttribute("download", `userhistory-${(new Date()).toISOString()}.json`);
            document.body.appendChild(downloadAnchorNode); // required for firefox
            downloadAnchorNode.click();
            downloadAnchorNode.remove();
        });
    }

    document.getElementById("import-history").onclick = function () {
        notie.confirm('Are you sure you want to do that? It will clear user history.', 'Yes', 'Cancel', function() {
            document.getElementById("userhistory-file").click();
            document.getElementById("userhistory-file").onchange =function(e) {
                const file = document.getElementById("userhistory-file").files[0];
                const reader = new FileReader();

                reader.onload = function(e) {
                    const fileContent = e.target.result;      
                    console.log(JSON.parse(fileContent));             
                    chrome.storage.local.set(JSON.parse(fileContent));
                    // var bl = JSON.parse(fileContent);
                    // bl['SITE'].unshift('chrome-ui://newtab')
                    // chrome.storage.local.get(['blacklist'], function(items) {
                    //     var blacklist = items['blacklist'];
                    //     b1['SITE'] = ['chrome-ui://newtab']
                    //     chrome.storage.local.set({'blacklist':blacklist});
                    // });

                   

                    // chrome.runtime.sendMessage({
                    //     "msg": 'setBlacklist',
                    //     "blacklist": bl
                    // });

                    notie.alert(1, 'Imported user history. Restarting the extension...', 2)
                    // setTimeout(function() {
                    //     chrome.runtime.reload()
                    // }, 2000);
                };

                if(typeof file !== "undefined")
                    reader.readAsText(file);
            };
            
        });
    }

    document.getElementById("search-history-btn").onclick = function () {
        getHistory(document.getElementById("search_history").value);
    }

    document.getElementById("export").onclick = function() {
        var tab = document.getElementById("blacklist_tbl");

        if(tab.rows[1].cells[0].innerText === "") {
            notie.alert(4, "There is no blacklist entry to export", 2);
        }
        else {
            notie.alert(4, "Exported Preferences.", 2);
            var indices = [];
            for (var i = 1; i < tab.rows.length; i++) {
                var row = tab.rows[i]
                if (row.cells[0].innerText === "") {
                    indices.push(i)
                }
            }

            for (var j = indices.length-1; j > -1; j--) {
                tab.deleteRow(indices[j]);
            }


            if (tab.rows.length == 1) {
                chrome.runtime.sendMessage({
                    "msg": 'setBlacklist',
                    "blacklist": []
                });
                add("SITE", "");
            } else {
                let b = {
                    'SITE': [],
                    'PAGE': [],
                    'REGEX': []
                }

                let c = {
                    'SITE': [],
                    'PAGE': [],
                    'REGEX': []
                }

                for(let i = 1; i < tab.rows.length; i++) {
                    if(tab.rows[i].cells[0].innerText.includes("chrome-ui://"))
                        c[tab.rows[i].cells[1].childNodes[0].value].push(tab.rows[i].cells[0].innerText)
                    else {
                        b[tab.rows[i].cells[1].childNodes[0].value].push(tab.rows[i].cells[0].innerText)
                        c[tab.rows[i].cells[1].childNodes[0].value].push(tab.rows[i].cells[0].innerText)
                    }
                    
                }

                chrome.runtime.sendMessage({
                    "msg": 'setBlacklist',
                    "blacklist": c
                });

                const dataStr = "data:text/json;charset=utf-8," + encodeURIComponent(JSON.stringify(b));
                const downloadAnchorNode = document.createElement('a');
                downloadAnchorNode.setAttribute("href",     dataStr);
                const today = new Date();
                downloadAnchorNode.setAttribute("download", `ignorelist-${today.toISOString()}.json`);
                document.body.appendChild(downloadAnchorNode); // required for firefox
                downloadAnchorNode.click();
                downloadAnchorNode.remove();
            }
        }        
    }

    document.getElementById("import").onclick = function () {
        notie.confirm('Are you sure you want to do that? It will clear current lists.', 'Yes', 'Cancel', function() {
            document.getElementById("blacklist-file").click();
            document.getElementById("blacklist-file").onchange =function(e) {
                const file = document.getElementById("blacklist-file").files[0];
                const reader = new FileReader();

                reader.onload = function(e) {
                    const fileContent = e.target.result;                   

                    var bl = JSON.parse(fileContent);

                    bl['SITE'].unshift('chrome-ui://newtab')
                    // chrome.storage.local.get(['blacklist'], function(items) {
                    //     var blacklist = items['blacklist'];
                    //     b1['SITE'] = ['chrome-ui://newtab']
                    //     chrome.storage.local.set({'blacklist':blacklist});
                    // });

                   

                    chrome.runtime.sendMessage({
                        "msg": 'setBlacklist',
                        "blacklist": bl
                    });

                    notie.alert(1, 'Imported blacked list. Restarting the extension...', 2)
                    setTimeout(function() {
                        chrome.runtime.reload()
                    }, 2000);
                };

                if(typeof file !== "undefined")
                    reader.readAsText(file);
            };
            
        });
    }

    
})();
