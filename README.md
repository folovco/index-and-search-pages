# Search past pages for any text on them

Extension for Chrome-based browsers( mostly works on Firefox too!) for flexible full-text browsing history search. **Press `f`, then `space` or `tab`, in the omnibar to start searching your previously visited websites**. 

Every time you visit a website in Chrome, the extension indexes all the text on the page so that the site can be easily found later. Then, for example, if you type `f <tab> mugwort`, this extension will show the websites you visited containing the text "mugwort"! We are working to publish the the extension to [Chrome Web Store](https://chrome.google.com/webstore/category/extensions) later in August, will update here with a link as soon as finished.

<img src="usage.gif" alt="Example Usage" width="880px"/>

## Examples

`before: "yesterday at 5pm" after: "three weeks ago" emscripten blog "anish athalye"` 
- Searches for websites that you browsed between yesterday at 5pm and 3 weeks ago containing the keywords "emscripten" and "blog" and "anish athalye"

`-"cat food" just "a dog"`
- Searches for websites you visited containing the keywords "just" and "a dog", and without the phrase "cat food".

`ethereum medium` 
- Searches for websites you visited in the last 2 weeks containing the keywords "ethereum" and "medium"

`ethereum medium after:11/29/2015 before:3/26/2016` 
- Searches for websites you visited between 11/29/2015 and 3/26/2016 containing the keywords "ethereum" and "medium"

## Transparent Installation
If you don't feel comfortable installing a Chrome extension that can read and modify all data on the websites you visit from the webstore (which is a wise consideration), you can clone it on your local machine, read through our code to verify that it is not malicious, and then install it as an unpacked local extension through the menu in `chrome://extensions/`. This way you also won't receive any automatic updates, improving your security further. 

## Preferences Page
To manage which URLs the extension can index or to delete websites from the index, go to the preferences page offered when you click the extension icon.

<!-- !["Extension Popup, highlighted"](http://i.imgur.com/w6cdWsc/gDrive.png) -->

## FAQ
### "Will this index my bank statements?"
Nope! We have a dank long blacklist of bank domains to avoid indexing the vast majority of banking websites. You can edit them and add your own in the preferences page. If you accidentally visit a page that is indexed that you don't want indexed, you can go into preferences and delete it from your indexed pages. 

### "Do you store any information remotely?"
No, all information is stored locally. No data leaves your computer over the network. You can verify this claim further with *[little-rat](https://github.com/dnakov/little-rat)* extension.

## More Details
- Use `before:date` and `after:date` to search your history in a certain time range
  - You can use natural language along with quotes to specify dates as well, e.g. `before:"yesterday at 5pm"`
- Use quotations to look for exact matches of strings containing whitespace
- Only documents containing all words will be returned
